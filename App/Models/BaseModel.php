<?php
namespace App\Models;

use App\Services\Database\DbConnection;

abstract class BaseModel{
	protected static $conn;
	protected static $table ;
	protected static $primary_key ;

	public function __construct() {
		self::$conn = new DbConnection();
	}



	public static function insert($data) {
		// User (id,user_name,user_email,created_at)
		// $data = ['user_name'=>'ali123' , 'user_email'=>'ali123@gmail.com'];
		// insert data into table
	}

	public static function find($id) {
		$sql = "select * from {static::table} where {static::primary_key}='$id'";
		return static::$conn->query($sql);
	}

	public static function findAll() {
		$sql = "select * from " . static::$table;
		echo $sql;
		return self::$conn->query($sql);
	}

	public static function update($data,$id) {

	}

	public function delete($id) {
		$sql = "delete from {$this->table} where {$this->primary_key}='$id'";
		return $this->conn->query($sql);
	}


}