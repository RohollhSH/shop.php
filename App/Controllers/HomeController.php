<?php
namespace App\Controllers;

use App\Services\View\View;

class HomeController{

	public function index($request) {
		$data = [];
		View::load('home.index',$data,'frontend-redfooter');
	}
	public function contact($request) {
		View::load('home.contact');
	}

}